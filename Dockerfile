FROM python:alpine
RUN apk add \
        build-base \
        git \
        libffi-dev \
        openssh-client \
        rsync \
        yq \
        zsh \
        curl
RUN pip install --no-cache-dir --no-compile ansible jinja2-cli
CMD [ "ansible-playbook", "--version" ]